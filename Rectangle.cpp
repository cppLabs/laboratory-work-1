#include "Rectangle.h"
#include <cassert>

Rectangle::Rectangle() : m_rect(rectangle_t {0, 0, point_t {0, 0}})
{
	assert(m_rect.width >= 0);
	assert(m_rect.height >= 0);
}

Rectangle::Rectangle(rectangle_t rectangle) 
{
	m_rect = rectangle;
	assert(m_rect.width >= 0);
	assert(m_rect.height >= 0);
}

Rectangle::Rectangle(const Rectangle &obj)
{
	m_rect = obj.m_rect;
	assert(m_rect.width >= 0);
	assert(m_rect.height >= 0);
}

Rectangle::~Rectangle() 
{
	
}

float Rectangle::getArea() const 
{
	return m_rect.width * m_rect.height;
}

rectangle_t Rectangle::getFrameRect() 
{
	return m_rect;
}

void Rectangle::move(float x, float y) 
{
	m_rect.pos.x += x;
	m_rect.pos.y += y;
}

void Rectangle::move(point_t movepos) 
{
	m_rect.pos = movepos;
}
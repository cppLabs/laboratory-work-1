#include "Circle.h"
#define _USE_MATH_DEFINES
#include <cmath>
#include <cassert>

Circle::Circle() : m_circ(circle_t {0, point_t{0, 0}}) 
{
	assert(m_circ.radius >= 0);
}

Circle::Circle(circle_t circle) 
{
	m_circ = circle;
	assert(m_circ.radius >= 0);
}

Circle::Circle(const Circle &obj) 
{
	m_circ = obj.m_circ;
	assert(m_circ.radius >= 0);
}

Circle::~Circle() 
{
	
}

float Circle::getArea() const 
{
	return m_circ.radius * m_circ.radius * M_PI;
}

rectangle_t Circle::getFrameRect() 
{
	rectangle_t rectFrame;
	rectFrame.width = 2*m_circ.radius;
	rectFrame.height = 2*m_circ.radius;
	rectFrame.pos = m_circ.pos;
	return rectFrame;
}

void Circle::move(float x, float y)
{
	m_circ.pos.x += x;
	m_circ.pos.y += y;
}

void Circle::move(point_t movepos) 
{
	m_circ.pos = movepos;
}
#include <stdio.h>
#include <iostream>
#include "base-types.h"
#include "shape.h"
#include "Rectangle.h"
#include "Circle.h"
using namespace std;

int main() {

	point_t position; 
	position.x = 0.232;
	position.y = 1.343;

	rectangle_t rectangle;
	rectangle.width = 2.5;
	rectangle.height = 3.5;
	rectangle.pos = position;

	circle_t circle;
	circle.radius = 6.5;
	circle.pos = position;

	cout << "circle pos : " << circle.pos.x << endl;

	Shape *shapes[2];

	shapes[0] = new Rectangle (rectangle);

	cout << shapes[0] -> getArea() << endl;

	rectangle_t frameRect = shapes[0] -> getFrameRect();

	cout << frameRect.width << endl;

	shapes[0] -> move(8.0, 9.0);

	shapes[1] = new Circle (circle);

	cout << "Circle area : "<< shapes[1] -> getArea() << endl;

return 0;	
}
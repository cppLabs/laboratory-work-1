#ifndef _CIRCLE_H_
#define _CIRCLE_H_

#include "shape.h"
#include "base-types.h"

class Circle: public Shape {
public: 
	Circle();
	Circle(circle_t circle);
	Circle(const Circle &obj);
	~Circle();
	float getArea() const;
	rectangle_t getFrameRect();
	void move(float x, float y);
	void move(point_t movepos);
private: 
	circle_t m_circ;
};

#endif